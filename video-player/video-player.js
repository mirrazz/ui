window.MirrazzVideoWrapper=(function(){
    var VirtualEventTarget=function VirtualEventTarget() {
        var events=[];
        this.addEventListener=function(o,f){
            events.push({name:o,func:f})
        }
        this.removeEventListener=function(o,f){
            var r=[];
            events.forEach((w)=>{
                if(!(w.name===o&&w.func===f)) {
                    r.push(w)
                }
            })
            events=r;
        }
        this.dispatchEvent=function(e){
            if(!event.type){throw new Error('d')}
            events.forEach(x=>{
                try {
                    if(x.name===e.type){
                        x.func(e);
                    }
                } catch (s){
                    console.error(s)
                }
            });
            try {
                this[`on${e.type}`](e)
            } catch (g){
               null
            }
            if(e.defaultPrevented){
                return false
            } else {
                return true
            }
        }
    }
    var wrapper=function(source,options) {
        try {
            var {loop,autoplay,speed,cover,width,height,controls}=options
        } catch (error) {
            var {loop,autoplay,speed,cover,width,height,controls}={};
        }
        var div=document.createElement("div");
        div.setAttribute(
            'style',
            `width:${width||'400px'};height:${height||'400px'};background:black;`
        );
        div.style.overflow='hidden';
        var video=document.createElement('video');
        video.playbackRate=isNaN(speed)?1:speed;
        if(loop) { video.loop=true }
        if(autoplay) { video.autoplay=true }
        video.poster=cover;
        video.src=source;
        video.style.width="100%";
        video.style.height="100%";
        var contain=document.createElement('div');
        contain.style.width="100%";
        contain.style.height="100%";
        contain.style.position='relative';
        div.appendChild(contain)
        contain.appendChild(video);
        var toolbar=document.createElement('div');
        toolbar.style.width="100%";
        toolbar.style.position="absolute";
        toolbar.style.left="0px";
        toolbar.style.bottom="0px";
        toolbar.style.display='flex';
        toolbar.style.flexDirection="row";
        function createButton(svg,click) {
            var d=document.createElement('div');
            d.innerHTML=svg;
            d.onclick=(event)=>{click({
                event:event,
                div:d,
                svg:d.querySelector('svg')
            })}
            d.style.cursor='pointer';
            d.style.padding="4px";
            toolbar.appendChild(d);
            return d
        };
        var playpause=createButton(`<svg height="24" width="24" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><!--! Font Awesome Pro 6.2.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. --><path fill="white" d="M73 39c-14.8-9.1-33.4-9.4-48.5-.9S0 62.6 0 80V432c0 17.4 9.4 33.4 24.5 41.9s33.7 8.1 48.5-.9L361 297c14.3-8.7 23-24.2 23-41s-8.7-32.2-23-41L73 39z"/></svg>`,function({svg}){
            if(video.paused) {
                video.play()
            } else {
                video.pause()
            }
        })
        contain.appendChild(toolbar);
        var target=new VirtualEventTarget();
        target.onplay=(event)=>{
            this.onplay(event)
        }
        target.onpause=(event)=>{
            this.onpause(event)
        }
        target.onload=(event)=>{
            this.onload(event)
        }
        target.onseek=(event)=>{
            this.onseek(event)
        }
        target.onend=(event)=>{
            this.onend(event)
        }
        this.dispatchEvent=function(g){target.dispatchEvent(g)}
        this.addEventListener=function(g,w){target.addEventListener(g,w)}
        this.removeEventListener=function(g,w){target.removeEventListener(g,w)};
        class MVWEvent extends Event {constructor(...args){super(...args)}};
        video.onplay=()=>{
            playpause.innerHTML=`<svg height="24" width="24" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><!--! Font Awesome Pro 6.2.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. --><path fill="white" d="M48 64C21.5 64 0 85.5 0 112V400c0 26.5 21.5 48 48 48H80c26.5 0 48-21.5 48-48V112c0-26.5-21.5-48-48-48H48zm192 0c-26.5 0-48 21.5-48 48V400c0 26.5 21.5 48 48 48h32c26.5 0 48-21.5 48-48V112c0-26.5-21.5-48-48-48H240z"/></svg>`
            var event=new MVWEvent('play',{
                cancelable: false,
                bubbles: false,
                composed:false,
            });
            target.dispatchEvent(event)
        }
        video.onended=()=>{
            var event=new MVWEvent('end',{
                cancelable: false,
                bubbles: false,
                composed:false,
            });
            target.dispatchEvent(event)
        }
        video.onpause=()=>{
            playpause.innerHTML=`<svg height="24" width="24" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><!--! Font Awesome Pro 6.2.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. --><path fill="white" d="M73 39c-14.8-9.1-33.4-9.4-48.5-.9S0 62.6 0 80V432c0 17.4 9.4 33.4 24.5 41.9s33.7 8.1 48.5-.9L361 297c14.3-8.7 23-24.2 23-41s-8.7-32.2-23-41L73 39z"/></svg>`;
            var event=new MVWEvent('pause',{
                cancelable: false,
                bubbles: false,
                composed:false,
            });
            target.dispatchEvent(event)
        }
        video.ontimeupdate=()=>{
            var event=new MVWEvent('seek',{
                cancelable: false,
                bubbles: false,
                composed:false,
            });
            target.dispatchEvent(event)
        }
        if(controls===false) {
            toolbar.style.display="none";
        }
        this.attach=function(node) {
            node.appendChild(div)
        }
        this.seekBy=function(time){
            video.currentTime+=time 
        }
        this.setLoop=function(mode) {
            video.currentTime=Boolean(mode)
        }
        this.position=function(){
            return video.currentTime
        }
        this.seekTo=function(time) {
            video.currentTime=time
        }
        this.play=function(){
            video.play();
        }
        this.pause=function(){
            video.pause()
        }
    };
    return wrapper
})()
